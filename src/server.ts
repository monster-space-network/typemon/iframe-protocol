import { Type, Callable } from '@typemon/types';
import generateUUID from 'uuid/v4';
//
import { PackagedMessage } from './packaged-message';
//
//
//
export class Server {
    public static bootstrap(handle: Server.Handle, options: Server.Options = {}): Server {
        return new Server(handle, options);
    }

    private readonly self: Window;
    private readonly id: string;
    private readonly allowedOrigins: ReadonlyArray<string | RegExp>;

    private listener: Callable<[MessageEvent], void>;

    public constructor(
        private readonly handle: Server.Handle,
        private readonly options: Server.Options
    ) {
        this.self = window;
        this.id = generateUUID();
        this.allowedOrigins = this.options.allowedOrigins || [];
        this.listener = (event: MessageEvent): void => this.onMessage(event);

        this.self.addEventListener('message', this.listener);
    }

    private isAllowedOrigin(origin: string): boolean {
        return this.allowedOrigins.some((allowedOrigin: string | RegExp): boolean => Type.isString(allowedOrigin) ? allowedOrigin === origin : allowedOrigin.test(origin));
    }

    private onMessage(event: MessageEvent): void {
        if (this.allowedOrigins.length > 0 && Type.isFalse(this.isAllowedOrigin(event.origin))) {
            return;
        }

        const packagedMessage: PackagedMessage = event.data;

        if (Type.isFalse(PackagedMessage.isPackagedMessage(packagedMessage))) {
            return;
        }

        if (packagedMessage.message === 'handshake') {
            const client: Window = event.source as Window;

            client.postMessage({
                clientId: packagedMessage.clientId,
                serverId: this.id,
                id: packagedMessage.id,
                message: 'handshake'
            }, event.origin);
        }

        if (packagedMessage.serverId !== this.id) {
            return;
        }

        this.executeHandle(event, packagedMessage);
    }

    private async executeHandle(event: MessageEvent, packagedMessage: PackagedMessage): Promise<void> {
        const response: void | unknown = await this.handle(packagedMessage.message);
        const client: Window = event.source as Window;

        client.postMessage({
            clientId: packagedMessage.clientId,
            serverId: this.id,
            id: packagedMessage.id,
            message: response
        }, event.origin);
    }

    public destory(): void {
        this.self.removeEventListener('message', this.listener);
    }
}
export namespace Server {
    export type Handle = (message: unknown) => void | Promise<void> | unknown | Promise<unknown>;

    export interface Options {
        readonly allowedOrigins?: ReadonlyArray<string | RegExp>;
    }
}
