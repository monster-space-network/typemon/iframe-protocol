import { Type, ReadonlyIndex } from '@typemon/types';
//
//
//
export interface PackagedMessage {
    readonly clientId: string;
    readonly serverId: string;

    readonly id: string;
    readonly message: unknown;
}
export namespace PackagedMessage {
    export function isPackagedMessage(value: unknown): value is PackagedMessage {
        if (Type.isNotObject(value)) {
            return false;
        }

        const { id, clientId, serverId }: ReadonlyIndex<string, unknown> = value;

        return Type.isString(clientId) && Type.isString(serverId) && Type.isString(id);
    }
}
