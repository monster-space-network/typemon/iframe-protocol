import { Type, Callable } from '@typemon/types';
import generateUUID from 'uuid/v4';
//
import { PackagedMessage } from './packaged-message';
//
//
//
type MessageListener = Callable<[PackagedMessage], void | Promise<void>>;

export class Client {
    private readonly self: Window;
    private readonly id: string;

    private serverId: string;

    private readonly responseListeners: Map<string, MessageListener>;
    private listener: Callable<[MessageEvent], void>;

    private destroyed: boolean;

    public constructor(
        private readonly server: Window,
        private readonly serverOrigin: string = '*'
    ) {
        this.self = window;
        this.id = generateUUID();
        this.serverId = '';
        this.responseListeners = new Map();
        this.listener = (event: MessageEvent): void => this.onMessage(event);
        this.destroyed = false;

        this.self.addEventListener('message', this.listener);
    }

    private onMessage(event: MessageEvent): void {
        if (Type.isNull(this.server)) {
            return;
        }

        if (this.serverOrigin !== '*' && event.origin !== this.serverOrigin) {
            return;
        }

        const packagedMessage: PackagedMessage = event.data;

        if (Type.isFalse(PackagedMessage.isPackagedMessage(packagedMessage))) {
            return;
        }

        if (packagedMessage.message === 'handshake') {
            this.serverId = packagedMessage.serverId;
        }

        if (packagedMessage.clientId !== this.id || packagedMessage.serverId !== this.serverId) {
            return;
        }

        const listener: MessageListener | undefined = this.responseListeners.get(packagedMessage.id);

        if (Type.isUndefined(listener)) {
            return;
        }

        this.responseListeners.delete(packagedMessage.id);

        listener(packagedMessage);
    }

    private waitResponse(id: string): Promise<PackagedMessage> {
        return new Promise((resolve: (packagedMessage: PackagedMessage) => void): void => {
            const listener: MessageListener = (packagedMessage: PackagedMessage): void => resolve(packagedMessage);

            this.responseListeners.set(id, listener);
        });
    }

    private handshake(): Promise<void> {
        return new Promise((resolve: () => void): void => {
            const packagedMessage: PackagedMessage = {
                clientId: this.id,
                serverId: '',
                id: generateUUID(),
                message: 'handshake'
            };

            this.waitResponse(packagedMessage.id).then((): void => resolve());
            this.server.postMessage(packagedMessage, this.serverOrigin);
        });
    }

    public async request<Message>(message: unknown): Promise<Message> {
        if (this.destroyed) {
            throw new Error('The client has been destroyed.');
        }

        if (Type.isEmptyString(this.serverId)) {
            await this.handshake();
        }

        return new Promise((resolve: (message: Message) => void): void => {
            const packagedMessage: PackagedMessage = {
                clientId: this.id,
                serverId: this.serverId,
                id: generateUUID(),
                message
            };

            this.waitResponse(packagedMessage.id).then((packagedMessage: PackagedMessage): void => resolve(packagedMessage.message as Message));
            this.server.postMessage(packagedMessage, this.serverOrigin);
        });
    }

    public destory(): void {
        this.self.removeEventListener('message', this.listener);
        this.responseListeners.clear();
        this.destroyed = true;
    }
}
