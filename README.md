# Iframe Protocol - [![npm-version](https://img.shields.io/npm/v/@typemon/iframe-protocol.svg)](https://www.npmjs.com/package/@typemon/iframe-protocol) [![npm-downloads](https://img.shields.io/npm/dt/@typemon/iframe-protocol.svg)](https://www.npmjs.com/package/@typemon/iframe-protocol)
> Secure and powerful promise-based messaging with iframes



## About
We like sso implementations and cool stuff using iframes.
But the native messaging system is a little trickier to use.
So we created a secure and powerful promise-based iframe messaging tool.
I hope this helps.



## Features
- See the [Wiki](https://gitlab.com/monster-space-network/typemon/iframe-protocol/wikis/home) for detailed feature descriptions and API.



## Installation
```
$ npm install @typemon/iframe-protocol
```
```typescript
import {
    /* Server class */
    Server,

    /* Client class */
    Client
} from '@typemon/iframe-protocol';
```



## Usage
### First, standardization the message.
```typescript
interface Action {
    readonly name: string;
    readonly data?: unknown;
}
```

### Create a handle to handle the request and bootstrap the server.
- Only communicate with clients that know their origin through the `allowedOrigins` option.
```typescript
async function handle(message: Action): Promise<unknown> {
    switch (message.name) {
        case 'get-access-token':
            return auth.getAccessToken();

        case 'renewal-access-token': {
            await auth.renewalAccessToken();

            return auth.getAccessToken();
        };

        case 'logout':
            await auth.logout();

        . . .
    }
}

Server.bootstrap(handle, { allowedOrigins: ['http://localhost:4201'] });
```

### Configure the client and start communication.
```typescript
const serverIFrame: HTMLIFrameElement = document.getElementById('server');
const client: Client = new Client(serverIFrame.contentWindow, 'http://localhost:4200');

const accessToken: string = await client.request({ name: 'get-access-token' });
const renewedAccessToken: string = await client.request({ name: 'renewal-access-token' });
await client.request({ name: '...', data: { ... } });
```
